import React, { useEffect, useState, useContext } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import './css/select.css';
import './css/components.css';
import './css/tailwind.css';
import './css/react-select.css';
import './css/tachyons.min.css';
import VirtualizedSelect from "react-virtualized-select";
Select.defaultProps = {
  title: "",
  option: [],
  selected: "",
  loading: false,
  name: "",
  disabled: false,
  required: false
};
Select.prototype = {
  title: PropTypes.string,
  option: PropTypes.array,
  selected: PropTypes.object,
  setSelect: PropTypes.func,
  name: PropTypes.string,
  clearable: PropTypes.bool,
  defaultMenuIsOpen: PropTypes.bool,
  optionHeight: PropTypes.number,
  height: PropTypes.number,
  placeholder: PropTypes.string,
  style: PropTypes.string,
  error: PropTypes.bool,
  errorID: PropTypes.string,
  maxHeight: PropTypes.number,
  disabled: PropTypes.bool,
  required: PropTypes.bool
};
function Select({
  title,
  option,
  setSelect,
  selected,
  loading,
  name,
  clearable,
  defaultMenuIsOpen,
  optionHeight,
  height,
  placeholder,
  style,
  error,
  errorID,
  maxHeight,
  disabled,
  className,
  size,
  required,
  props
}) {
  return /*#__PURE__*/React.createElement("div", {
    className: "tw-flex tw-flex-wrap tw-w-full"
  }, /*#__PURE__*/React.createElement("style", null, `
                    .Select-menu-outer{
                        border-color:#009DDE;
                        z-index:3;
                    }
                     .Select-menu-outer div{
                        font-size: 18px;
                        font-family: "DBHelvethaica", sans-serif;
                        line-height:1rem;
                        border-color:#009DDE;
                    }
                    .VirtualizedSelectSelectedOption{
                        color:#009DDE;
                    }
                    .Select-placeholder{
                        font-size: 20px;
                        font-family: "DBHelvethaica", sans-serif;
                    }
                    .Select-value-label{
                        font-size: 20px;
                        font-family: "DBHelvethaica", sans-serif;
                    }
                    .Items{
                        scrollbar-width: thin;
                    }
                    .Select-value{
                        cursor: ${disabled ? "not-allowed !important" : "pointer"}
                    }

                `), title && /*#__PURE__*/React.createElement("div", {
    className: "tw-py-2 tw-font-db tw-text-lg tw-font-bold tw-text-dark-gray"
  }, title, required && /*#__PURE__*/React.createElement("label", {
    className: "tw-font-db tw-text-lg tw-text-red"
  }, "\xA0*")), /*#__PURE__*/React.createElement("main", {
    className: "tw-w-full tw-relative"
  }, loading && /*#__PURE__*/React.createElement("div", {
    className: `${size === "small" ? "tw-h-[30px]" : "tw-h-10"} tw-absolute tw-w-full tw-flex tw-items-center tw-justify-end  tw-scroll-pr-2.5`,
    style: {
      zIndex: "2"
    }
  }, /*#__PURE__*/React.createElement("div", {
    className: "tw-bg-gray-porcelain tw-px-3 tw-mr-1"
  }, /*#__PURE__*/React.createElement(FontAwesomeIcon, {
    className: "tw-h-3 tw-text-blue",
    icon: faSpinner,
    pulse: true
  }))), /*#__PURE__*/React.createElement(VirtualizedSelect, {
    defaultMenuIsOpen: defaultMenuIsOpen || false,
    name: name,
    placeholder: placeholder || "โปรดเลือก",
    className: `!tw-text-black !tw-font-db !tw-text-xl input-reset  !tw-border !tw-rounded focus:!tw-shadow-input-focus ${disabled ? "!tw-bg-gray-athens !tw-border-gray-oslo !tw-text-gray-oslo !tw-cursor-not-allowed" : size === "small" ? "!tw-bg-gray-porcelain !tw-border-gray-mystic !tw-text-black !tw-cursor-pointer !tw-h-[30px]" : `!tw-bg-gray-porcelain ${error ? '!tw-border-red' : '!tw-border-blue'} !tw-text-black !tw-cursor-pointer !tw-h-10`}`,
    style: {
      height: size === "small" ? "30px" : "40px",
      opacity: "1",
      border: "none",
      margin: size === "small" ? "" : "1.5px 0px",
      style
    },
    onChange: selectValue => setSelect(selectValue),
    options: option,
    value: selected,
    clearable: clearable || false,
    optionHeight: optionHeight,
    height: height,
    scrollbarWidth: "thin",
    maxHeight: maxHeight,
    disabled: disabled
  })), error && /*#__PURE__*/React.createElement("div", {
    id: errorID,
    className: "tw-text-red tw-text-lg tw-font-db"
  }, error));
}
export default Select;