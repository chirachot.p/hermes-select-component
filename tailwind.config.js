module.exports = {
  prefix: 'tw-',
  content: [
      "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
      extend: {
          colors: {
              electric: '#db00ff',
              ribbon: '#0047ff',
              // Create DEFAULT color

              // Other color
              lime: {
                  DEFAULT: '#84CC16', //lime-500
                  '200': '#d9f99d',
                  '300': '#bef264',
                  '400': '#a3e635'
              },
              red: {
                  DEFAULT: '#EF4444',//red 500
                  '200': '#fecaca'
              },
              yellow: {
                  DEFAULT: '#FACC15', //yellow-400
                  '100': '#fef9c3'
              },
              orange: {
                  DEFAULT: '#FB923C', //orange-400
                  '100': '#FFEDD5' //orange-100
              },

              //Primary Color
              blue: {
                  DEFAULT: '#009DDE',
                  'light': '#009DDE',
                  'dark': '#26285E',
                  'ptt': '#007EB5',
                  'regent-st': '#A4D7E4',
                  'iceberg': '#D7EDF2',
                  'twilight': '#F4FDFF',
              },

              // Secondary color
              'black': '#1D2122',
              'dark-gray': '#494F52',
              gray: {
                  DEFAULT: '#8B969A',
                  'oslo': '#8B969A',
                  'tiara': '#C8CFD2',
                  'geyser': '#D9E1E4',
                  'mystic': '#E9F0F2',
                  'athens': '#F8F9FA',
                  'porcelain': '#FCFDFD',
              },

              // Other color
              // red: '#EF4444', //red-500
              // green: '#84CC16', //lime-500
              // yellow: '#FACC15', //yellow-400
              // orange: '#FB923C', //orange-400

              highlight: {
                  blue: '#F0F9FF',
                  red: '#FFF1F2',
                  green: '#F0FDFA',
                  yellow: '#F59E0B',
              },

              hover: {
                  blue: '#007EB5',
                  red: '#B91C1C',
                  green: '#4D7C0F',
                  yellow: '#EAB308',
                  gray: '#4B5563',
                  glass: 'rgba(255,255,255,0.4)'
              },

              'glass': 'rgba(255,255,255,0.1)',
              'screen': 'rgba(0, 0, 0, 0.4)',
          },
          boxShadow: {
              base: '0px 4px 8px rgba(0, 0, 0, 0.04), 0px 0px 2px rgba(0, 0, 0, 0.06), 0px 0px 1px rgba(0, 0, 0, 0.04)',
              'card-hover': '0px 0px 8px #3DAEE3',
              'input-focus': '0px 0px 7px rgba(0, 157, 222, 0.5)',
              'nav-left': '0px 4px 4px rgba(0, 0, 0, 0.25)',
              'card': '0px 4px 4px rgba(0, 0, 0, 0.1)',
              'dashboard': '0px 3px 4px rgba(0, 0, 0, 0.05)'
          },
          height: {
              '100vh': '100vh',
          },
          width: {
              "fit": "fit-content"
          },
          gridTemplateColumns: {
              'auto-fill': 'repeat(auto-fill, minmax(300px, 1fr))'
            }
      },
      borderWidth: {
          DEFAULT: '1px',
          'input': '0.5px',
          '0': '0',
          '2': '2px',
          '3': '3px',
          '4': '4px',
          '6': '6px',
          '8': '8px'
      },
      placeholderColor: {
          'primary': '#C8CFD2',
          'secondary': '#ffed4a',
          'danger': '#e3342f',
      },
      fontFamily: {
          'db': ['"DBHelvethaica"'],
          'print': ['"AngsanaUPC"']
      },
      screens: {
          'sm': '640px',
          'md': '768px',
          'lg': '1024px',
          'xl': '1280px',
          '2xl': '1536px',
          // Add style old size
          'mobile': '0px',
          'window': '1366px'
      }
  },

  corePlugins: {
      preflight: false,
  },

  variants: {
      extend: {
          display: ['group-hover'],
      }
  },

  plugins: ["babel-plugin-styled-components"]
}
